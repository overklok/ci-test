<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(['url', 'form']);
    }

    /**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
    {
        $content = $this->load->view('main/index', [], true);

		$this->load->view('layouts/main', [
            'title' => 'Загрузить файл',
            'content' => $content,
        ]);
	}

    public function upload()
    {
        $config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'gif|jpg|png';

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('userfile')) {
            $data = ['error' => $this->upload->display_errors()];
        } else {
            $this->load->library('table');

            $data = ['success' => $this->upload->data()];
        }

        $content = $this->load->view('main/upload', $data, true);

        $this->load->view('layouts/main', [
            'title' => 'Загрузка файла',
            'content' => $content,
        ]);
    }
}
