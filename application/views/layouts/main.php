<?php
/** @var $content string */
/** @var $title string */
?>

<!DOCTYPE html>
<html lang = "ru">

<head>
    <title><?=$title?></title>
    <meta charset = "utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/custom.css">

    <script type="application/javascript" src="/assets/js/jquery-3.1.0.min.js"></script>
    <script type="application/javascript" src="/assets/js/bootstrap.min.js"></script>
</head>


    <body>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="/">Тестовое задание</a>
                </div>
                <ul class="nav navbar-nav">
                    <li class="active"><a href="/">Форма загрузки файла</a></li>
                </ul>
            </div>
        </nav>

        <div class="container">
            <h1><?=$title ?></h1>
            <?=$content ?>
        </div>

        <footer class="footer">
            <div class="container">
                <p class="text-muted">
                    Страница сгенерирована за <strong>{elapsed_time}</strong> сек.
                    <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?>
                </p>
            </div>
        </footer>
    </body>
</html>
