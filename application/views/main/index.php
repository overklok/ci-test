<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<p>Поддерживаются типы файлов <b>gif, jpg, png</b></p>

<form action="/index.php/main/upload" method="post" enctype="multipart/form-data">
    <input type="file" name="userfile">
    <br><br>
    <input class="btn btn-success btn-sm" type="submit" value="Загрузить файл" name="submit">
</form>