<?php
/* @var $error string */
/* @var $success array */

?>

<?php if (isset($error)): ?>
    <div class="alert alert-danger">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Ошибка!</strong><?=$error?>
    </div>
<?php endif; ?>

<?php if (isset($success)): ?>
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        Файл успешно загружен на сервер!
    </div>

    <?php $this->table->set_caption(
        'Сведения о файле'
    ) ?>

    <?php $this->table->set_heading(
        'Атрибут', 'Значение'
    ) ?>

    <?php $this->table->set_template(
        ['table_open' => '<table class="table" border="0" cellpadding="4" cellspacing="0">']
    ) ?>

    <?=$this->table->generate([

            ['Имя файла', $success['file_name']],
            ['Тип', $success['file_type']],
            ['Имя исходного файла', $success['orig_name']],
            ['Размер', $success['file_size']],
            ['Размеры изображения', $success['image_size_str']],
        ]
    );
    ?>

<?php endif; ?>

<a href="/index.php" class="btn btn-primary">Вернуться</a>

